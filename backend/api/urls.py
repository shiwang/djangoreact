from django.conf.urls import url
from rest_framework.authtoken import views as drf_views

from .views import TodoListView
app_name = 'api'

urlpatterns = [
    url('auth$', drf_views.obtain_auth_token, name='auth'),
    url('todo/$', TodoListView.as_view(), name='todo-list-view'),
    url(r'get_auth_token/$', drf_views.obtain_auth_token, name='get_auth_token'),
]