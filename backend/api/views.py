from django.shortcuts import render

from rest_framework.generics import ListAPIView
from rest_framework import viewsets
from .models import Todo
from .serializers import TodoSerializers


class TodoListView(ListAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializers