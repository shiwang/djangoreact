// file: src/util/Auth.js
import axios from 'axios';
import _ from 'lodash';
import store from '../store';
import { setToken } from '../actions'
import { URL, LOGIN } from '../config/Api';

export function InvalidCredentialsException(message) {
    this.message = message;
    this.name = 'InvalidCredentialsException';
}

export function login(username, password) {
  return axios
    .post(URL + LOGIN, {
      username,
      password,
    })
    .then(function (response) {
       store.dispatch(setToken(response.data.token));
    })
    .then(function () {
        get_todo()
       loggedIn()
    })
    .catch(function (error) {
      // raise different exception if due to invalid credentials
      if (_.get(error, 'response.status') === 400) {
        throw new InvalidCredentialsException(error);
      }
      throw error;
    });
}

export function test() {
axios.get('https://en.wikipedia.org/w/api.php?origin=*')
     .then((response) => {
       console.log(response);
     })
    .catch((error)=>{
       console.log(error);
    });
}
export function get_todo() {
    const AuthStr = 'Token '.concat(store.getState().token);
    return axios
        .get('http://localhost:8000/api/todo/',{
          headers: {
              Authorization: AuthStr,
          }
        })
        .then(function (response) {
            console.log(response)
        })
        .catch(function (error) {
          // raise different exception if due to invalid credentials
          if (_.get(error, 'response.status') === 400) {
            throw new InvalidCredentialsException(error);
          }
          throw error;
        });
}

export function loggedIn() {
    console.log("token obtained",store.getState().token);
  return store.getState().token == null;
}
